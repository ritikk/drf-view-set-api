from rest_framework import viewsets
from .models import Student
from .serialzers import StudentSerializer
from rest_framework.response import Response


class StudentViewSet(viewsets.ViewSet):
    def list(self, request):
        stu = Student.objects.all()
        serializer = StudentSerializer(stu, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        id = pk
        if id is not None:
            stu = Student.objects.get(id=id)
            serializer = StudentSerializer(stu)
            return Response(serializer.data)

    def create(self, request):
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                'msg': 'Data Creation Successful'
            })
        return Response(serializer.errors)

    def update(self, request, pk=None):
        id = pk
        if id is not None:
            stu = Student.objects.get(id=id)
            serializer = StudentSerializer(stu, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({
                    'msg': 'Data updation successfull'
                })
            return Response(serializer.errors)

    def destroy(self, request, pk):
        id = pk
        stu = Student.objects.get(id=id)
        stu.delete()
        return Response({
            'msg': 'Data Deletion Successfull'
        })
